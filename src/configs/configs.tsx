const defaultCards = {
    text: {
        type:"text",
        title:"Новый заголовок",
        data:"Здесь будет <strong>Ваш</strong> текст<br/><p style='color: cornflowerblue'>Тоже текст,но синего цвета</p>"
    },
    photo: {
        type:"photo",
        title:"Новый заголовок",
        data:"https://upload.wikimedia.org/wikipedia/commons/thumb/b/b6/Image_created_with_a_mobile_phone.png/1200px-Image_created_with_a_mobile_phone.png"
    },
    video: {
        type:"video",
        title:"Новый заголовок",
        data:"https://www.youtube.com/watch?v=9rC45C5iT34"
    },
    chart: {
        type:"chart",
        title:"Новый заголовок",
        data:{
            xTitle: "XTitle",
            yTitle: "YTitle",
            xTitles: ["Quarter 1", "Quarter 2", "Quarter 3", "Quarter 4"],
            data: [ 100, 400,300,400]
        }
    }
}

const formsConfig = {
    text: {
        title:{
            label:"Заголовок",
            type:"input"
        },
        data:{
            label:"Текст",
            type:"textarea"
        }
    },
    photo: {
        title:{
            label:"Заголовок",
            type:"input"
        },
        data:{label:"Выберите фото",
                type:"image"
        }
    },
    video: {
        title:{
            label:"Заголовок",
            type:"input"
        },
        data:{label:"Ссылка",
            type:"input"
        }
    },
    chart: {
        title:{
            label:"Заголовок",
            type:"input"
        },
        data:[
            {label:"Название оси Х", type:"input"},
            {label:"Название оси Y", type:"input"},
            {label:"Заголовки столбцов", type:"textarea"},
            {label:"Значения", type:"textarea"}
        ]
    }
}

export  const configs={
    defaultCards,
    formsConfig
}