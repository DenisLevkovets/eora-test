const requestStatus = {
    REQUEST: "_REQUEST",
    SUCCESS: "_SUCCESS",
    FAILURE: "_FAILURE"
};

const dashboardActions = {
    GET:"GET",
    GET_ALL:"GET_ALL",
    SET:"SET",
    DELETE:"DELETE"
}

const cards = {
    GET_CARDS: "GET_CARDS",
    ADD_CARD: "ADD_CARD",
    EDIT_CARD: "EDIT_CARD",
    CHANGE_CARD_ORDER: "CHANGE_CARD_ORDER",
    DELETE_CARD:"DELETE_CARD"
}


export default {
    requestStatus,
    dashboardActions,
    cards
}
