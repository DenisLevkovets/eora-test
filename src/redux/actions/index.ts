import dashboard from './dashboardActions'
import cards from './cardActions'

export default {
    dashboard,
    cards
}