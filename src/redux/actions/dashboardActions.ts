import constants from '../constants';
import {dashboardProps} from "../../configs/Props";
import utils from './utils'
import axios from 'axios'
const {request, failure, success} = utils

const api_url = "https://sheltered-refuge-69809.herokuapp.com"

function getDashboards() {
    return (dispatch) => {
        dispatch(request(constants.dashboardActions.GET_ALL));
        return axios.get(api_url+'/getDashboards').then(res => {
            dispatch(success(constants.dashboardActions.GET_ALL, res.data));
            return res.data
        }).catch(err => {
            dispatch(failure(constants.dashboardActions.GET_ALL, err));
        })
    }
}

function getDashboard(id) {
    return (dispatch) => {
        dispatch(request(constants.dashboardActions.GET));
        return axios.get(api_url+'/getDashboard', {
            params: {
                id: id,
            },
        }).then(res => {
            dispatch(success(constants.dashboardActions.GET, res.data));
            return res.data
        }).catch(err => {
            dispatch(failure(constants.dashboardActions.GET, err));
        })
    }
}

function editDashboard(dashboard: dashboardProps, newTitle: string) {
    const body ={
        dashboard:dashboard,
        title: newTitle
    }
    return (dispatch) => {
        dispatch(request(constants.dashboardActions.SET));
        axios.patch(api_url+'/editDashboard', body).then(res => {
            dispatch(success(constants.dashboardActions.SET, res.data));
        }).catch(err => {
            dispatch(failure(constants.dashboardActions.SET, err));
        })
    }
}

function deleteDashboard(id:number) {
    return (dispatch) => {
        dispatch(request(constants.dashboardActions.DELETE));
        return axios.get(api_url+'/deleteDashboard', {params:{id:id}}).then(res => {
            dispatch(success(constants.dashboardActions.DELETE, res.data));
            return res.data[0].id
        }).catch(err => {
            dispatch(failure(constants.dashboardActions.DELETE, err));
        })
    }
}


export default {
    getDashboard,
    getDashboards,
    editDashboard,
    deleteDashboard
}
