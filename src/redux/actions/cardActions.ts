import constants from '../constants';
import {cardProps} from "../../configs/Props";
import utils from "./utils";
import axios from 'axios'

const {request, failure, success} = utils


const api_url = "https://sheltered-refuge-69809.herokuapp.com"

function getCards(id: number) {
    return (dispatch) => {
        dispatch(request(constants.cards.GET_CARDS));
        return axios.get(api_url + '/getCards', {
            params: {
                id: id
            }
        }).then(res => {
            dispatch(success(constants.cards.GET_CARDS, res.data));
            return res.data
        }).catch(err => {
            dispatch(failure(constants.cards.GET_CARDS, err));
            return []
        })
    }
}

function addCard(card:cardProps) {
    const body = {card: card}
    return (dispatch) => {
        dispatch(request(constants.cards.ADD_CARD));
        return axios.post(api_url + '/addCard', body).then(res => {
            dispatch(success(constants.cards.ADD_CARD, res.data));
            return res.data
        }).catch(err => {
            dispatch(failure(constants.cards.ADD_CARD, err));
            return []
        })
    }
}

function deleteCard(card:cardProps) {
    const body = {card: card};
    return (dispatch) => {
        dispatch(request(constants.cards.DELETE_CARD));
        return axios.post(api_url + '/deleteCard', body).then(res => {
            dispatch(success(constants.cards.DELETE_CARD, res.data));
            return res.data
        }).catch(err => {
            dispatch(failure(constants.cards.DELETE_CARD, err));
        })
    }
}

function editCard(newCard:cardProps) {
    const body = {newCard: newCard}

    return (dispatch) => {
        dispatch(request(constants.cards.EDIT_CARD));
        return axios.patch(api_url + '/editCard', body).then(res => {
            dispatch(success(constants.cards.EDIT_CARD, res.data));
            return res.data
        }).catch(err => {
            dispatch(failure(constants.cards.EDIT_CARD, err));
        })
    }
}

function changeCardOrder(newCardOrder: cardProps[], dashboardId: number) {
    const body = {newCardOrder: newCardOrder}

    return (dispatch) => {
        dispatch(request(constants.cards.CHANGE_CARD_ORDER));
        axios.post(api_url + '/changeCardOrder', body, {params: {id: dashboardId}}).then(res => {
            dispatch(success(constants.cards.CHANGE_CARD_ORDER, res.data));
        }).catch(err => {
            dispatch(failure(constants.cards.CHANGE_CARD_ORDER, err));
        })
    }
}


export default {
    getCards,
    addCard,
    editCard,
    changeCardOrder,
    deleteCard
}
