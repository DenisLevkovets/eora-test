import constants from '../constants';

const initialState = {

};


export const dashboard = (state = initialState, action) => {
    switch (action.type) {
        case constants.dashboardActions.GET+ constants.requestStatus.SUCCESS:
            return  {
                ...state,
                dashboard:action.payload
            };
        case constants.dashboardActions.GET_ALL+ constants.requestStatus.SUCCESS:
            return {
                ...state,
                dashboards: action.payload
            };
        case constants.dashboardActions.SET+ constants.requestStatus.SUCCESS:
            return  {
                ...state,
                dashboard:action.payload.dashboard,
                dashboards: action.payload.dashboards
            };
        case constants.dashboardActions.DELETE+ constants.requestStatus.SUCCESS:
            return  {
                ...state,
                dashboards: action.payload
            };
        default:
            return state;
    }
};



