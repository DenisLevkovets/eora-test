import {combineReducers} from 'redux';
import {dashboard} from './dashboardReducers'
import {cards} from './cardReducers'

const rootReducer = combineReducers({
    dashboard,
    cards
});

export default rootReducer;
