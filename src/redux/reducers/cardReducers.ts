import constants from '../constants';

const initialState = [];


export const cards = (state = initialState, action) => {
    switch (action.type) {
        case constants.cards.GET_CARDS+ constants.requestStatus.SUCCESS:
            return  action.payload;
        case constants.cards.ADD_CARD+ constants.requestStatus.SUCCESS:
            return  action.payload;
        case constants.cards.DELETE_CARD+ constants.requestStatus.SUCCESS:
            return  action.payload;
        case constants.cards.EDIT_CARD+ constants.requestStatus.SUCCESS:
            return  action.payload;
        case constants.cards.CHANGE_CARD_ORDER+ constants.requestStatus.SUCCESS:
            return  action.payload;
        default:
            return state;
    }
};



