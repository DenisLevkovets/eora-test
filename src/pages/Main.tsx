import React, {useState, useEffect} from 'react';
import './Main.scss'
import Left from "../components/Sidebars/Left";
import Right from "../components/Sidebars/Right";
import Dashboard from "../components/Dashboard";
import actions from "../redux/actions";
import {connect} from "react-redux";
import {cardProps, dashboardProps} from '../configs/Props'
import {Spin} from "antd";

interface Props {
    dashboard: dashboardProps;

    getDashboard(id: number): dashboardProps;
    editCard(card: cardProps): cardProps[];
    editDashboard(dashboard: dashboardProps, newTitle: string): void;
}


function Main(props: Props) {
    // Текущая выбранная карта
    const [card, setCard] = useState()
    // Текущий выбранный дашбоард
    const [id, setId] = useState<number>(1)
    // Список карт дополнительно хранится в локальном стейте,
    // чтобы после смены порядка не ждать ответ с сервера для плавности анимации
    let [fastCards, setFastCards] = useState<cardProps[]>([])


    useEffect(() => {
        (async function () {
            await props.getDashboard(id);
        })();
    }, [id])


    if (props.dashboard === undefined) return <Spin className={"spin"} size={"large"}/>
    return (
        <div className={'container'}>
            <Left {...props} setId={setId} id={id} setCard={setCard}/>
            <Dashboard {...props} fastCards={fastCards} setFastCards={setFastCards} setCard={setCard}
                       activeCardId={card ? card.id : -1}/>
            <Right {...props} card={card} setCard={setCard} setFastCards={setFastCards}/>
        </div>
    )
}

function mapStateToProps(state) {
    return {
        dashboard: state.dashboard.dashboard
    }
}

function mapDispatchToProps(dispatch) {
    return {
        getDashboard: (id: number) => dispatch(actions.dashboard.getDashboard(id)),
        editDashboard: (dashboard: dashboardProps, newTitle: string) => dispatch(actions.dashboard.editDashboard(dashboard, newTitle)),
        editCard: (card: cardProps) => dispatch(actions.cards.editCard(card))
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(Main);