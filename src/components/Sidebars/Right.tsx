import React, {useEffect, useState} from "react";
import './Right.scss'
import {Form, Input, Button, message} from 'antd';
import {cardProps, dashboardProps} from "../../configs/Props";
import {configs} from "../../configs/configs";
import UploadReady from '../Upload'

interface Props {
    dashboard: dashboardProps;
    card: cardProps

    editCard(card: cardProps): cardProps[];
    editDashboard(dashboard: dashboardProps, newTitle: string): void;
    setFastCards(cards: cardProps[]): void;
    setCard(card: cardProps): void;
}

const layout = {
    labelCol: {span: 8},
    wrapperCol: {span: 16},
};

const validValue = (value, type) => {
    let valid = true;
    value.forEach(x => {
        if (typeof x !== type) {
            valid = false
            message.error("Неправильный формат ввода")
        }
    })
    return valid
}

const convertToArray = (arrayLike, type) => {
    return Array.from(arrayLike.split('"').join('').replace('[', '').replace(']', '').split(',').map(x => Number.isNaN(+x) || type === 'string' ? x : +x));
}


const renderSwitch = (param, setPhoto?) => {
    switch (param) {
        case 'input':
            return <Input/>
        case 'textarea':
            return <Input.TextArea rows={5}/>
        case 'image':
            return <UploadReady setPhoto={setPhoto}/>
        default:
            return <div/>
    }
}

function Right(props: Props) {
    const [form] = Form.useForm();
    const [photo, setPhoto] = useState();

    useEffect(() => {
        // Ресет полей формы для установки новых значений карточки или нового заголовка дашбоарда
        form.resetFields();
    }, [props.card, props.dashboard]);

    const onFinish = async (values) => {
        if (Object.values(values).length === 1) {
            // Зачем напрягаться, если можно не напрягаться
            props.editDashboard(props.dashboard, values.dashboardTitle)
        } else if (Object.values(values).length > 1) {

            if (values.dashboardTitle) props.editDashboard(props.dashboard, values.dashboardTitle);

            let updatedCard = Object.assign({}, props.card);
            let isValid = true

            Object.keys(configs.formsConfig[props.card.type]).forEach(key => {
                if (typeof updatedCard[key] === 'object') {
                    let keys = Object.keys(updatedCard[key])
                    for (let i = 0; i < keys.length; i++) {
                        let dataKey = keys[i]
                        if (Array.isArray(updatedCard[key][dataKey])) {
                            let newValue = convertToArray(values[dataKey], dataKey === 'xTitles' ? 'string' : 'number')
                            if (validValue(newValue, dataKey === 'xTitles' ? 'string' : 'number')) {
                                updatedCard[key][dataKey] = newValue
                            } else {
                                isValid = false
                                break
                            }
                        } else updatedCard[key][dataKey] = values[dataKey]
                    }
                } else updatedCard[key] = values[key];
            })

            if (props.card.type === 'photo') {
                updatedCard.data = photo;
            }

            if (updatedCard.type === 'chart' && updatedCard.data['xTitles'].length !== updatedCard.data['data'].length) {
                message.error("Количество значений не совпдает с количество слов")
                isValid = false
            }

            if (isValid) {
                props.setCard(updatedCard)
                props.setFastCards(await props.editCard(updatedCard));
            }


        }
    };


    let cardDescription;
    let dataDescription;
    if (props.card !== undefined) {
        let formConfig = configs.formsConfig[props.card.type]
        cardDescription = Object.keys(formConfig).map((key, index) => {
                if (!(typeof props.card[key] === 'object')) {
                    return (
                        <Form.Item
                            key={index}
                            name={key}
                            label={formConfig[key].label}
                            initialValue={props.card[key]}
                        >
                            {renderSwitch(formConfig[key].type, setPhoto)}
                        </Form.Item>
                    )
                } else {
                    // Если у нас дата вложенный объект, то заполням отдельный массив с отдельными Form Items
                    dataDescription = Object.keys(props.card.data).map((dataKey, index) => {
                        return (<Form.Item
                                key={index}
                                name={dataKey}
                                label={formConfig.data[index].label}
                                initialValue={Array.isArray(props.card.data[dataKey]) ? JSON.stringify(props.card.data[dataKey]) : props.card.data[dataKey]}
                            >
                                {renderSwitch(formConfig.data[index].type)}
                            </Form.Item>
                        )
                    })
                }
            }
        )
    }


    return (
        <div className={'right'}>
            <div className={"formHeader"}><h3>Свойства</h3></div>
            <Form
                {...layout}
                form={form}
                name="basic"
                className={"form"}
                initialValues={{remember: true}}
                onFinish={onFinish}
            >

                <Form.Item
                    label="Название Dashboard"
                    name="dashboardTitle"
                    initialValue={props.dashboard.title}
                >
                    <Input/>
                </Form.Item>
                {cardDescription}
                {dataDescription}
                <Form.Item wrapperCol={{span: 12, offset: 6}}>
                    <Button type="primary" htmlType="submit">
                        Сохранить
                    </Button>
                </Form.Item>
            </Form>
        </div>
    )
}


export default Right;