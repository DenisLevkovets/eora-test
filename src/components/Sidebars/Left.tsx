import React, {useEffect} from "react";
import './Left.scss'
import actions from "../../redux/actions";
import {connect} from "react-redux";
import {dashboardProps} from "../../configs/Props";
import axios from "axios";
import {PlusOutlined} from '@ant-design/icons';
import CloseIcon from '@material-ui/icons/Close';
import { IconButton } from '@material-ui/core';

interface Props {
    dashboards: dashboardProps[];
    id:number;

    getDashboards(): dashboardProps[];
    deleteDashboard(id:number):number;
    setId(id: number): void;
    setCard(card:any):void;
}

function Left(props: Props) {
    let {dashboards} = props

    const addDashboard = () => {
        axios.get('https://sheltered-refuge-69809.herokuapp.com/addDashboard').then(res => {
            props.setId(+res.data)
            props.getDashboards()
            props.setCard(undefined)
        }).catch(err => {
            console.log(err)
        })

    }
    useEffect(() => {
        (async function () {
            console.log(1)
            await props.getDashboards()
        })()
    }, [])
    if (dashboards === undefined) return  <div className={'left'}/>
    return (
        <div className={'left'}>
            {dashboards.map((dashboard, index) => (
                <div key={index} className={dashboard.id===props.id?"dashboardItem active":"dashboardItem"} onClick={() => {
                    props.setId(dashboard.id)
                    props.setCard(undefined)
                }}>
                    {dashboards.length>1?<IconButton className={"closeButton"} size={'small'} onClick={async ()=>{
                        props.setId(await props.deleteDashboard(dashboard.id))
                    }}>
                        <CloseIcon className={"closeButtonIcon"}/>
                    </IconButton>:true}
                    <p> {dashboard.title}</p>
                </div>
            ))}
            <div  className={"addBtn"} onClick={() => addDashboard()}><PlusOutlined/></div>
        </div>
    )
}

function mapStateToProps(state) {
    return {
        dashboards: state.dashboard.dashboards
    }
}

function mapDispatchToProps(dispatch) {
    return {
        getDashboards: () => dispatch(actions.dashboard.getDashboards()),
        deleteDashboard: (id:number) => dispatch(actions.dashboard.deleteDashboard(id))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Left);