import React from 'react'
import {
    VictoryBar, VictoryChart, VictoryAxis,
    VictoryTheme, VictoryLabel
} from 'victory';

interface Props {
    data: any;
}


function Chart(props: Props) {
    let {data} = props
    let values = data.data.map((value, index) => {
        return {x: index + 1, y: value}
    })
    return (
        <VictoryChart
            theme={VictoryTheme.material}
            domainPadding={20}
            style={{parent: {zIndex: 1}}}
        >
            <VictoryAxis
                label={data.xTitle}
                tickValues={Array.from({length: data.data.length}, (_, i) => i + 1)}
                tickFormat={data.xTitles}
                axisLabelComponent={<VictoryLabel dy={30}/>}
            />
            <VictoryAxis
                dependentAxis
                tickFormat={(x) => x}
                label={data.yTitle}
                axisLabelComponent={<VictoryLabel dy={-30}/>}

            />
            <VictoryBar
                data={values}
                x="x"
                y="y"
            />
        </VictoryChart>
    )
}

export default Chart;