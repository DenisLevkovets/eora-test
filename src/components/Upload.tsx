import React, {useState} from "react";
import {Upload, message, Button} from 'antd';
import {UploadOutlined} from '@ant-design/icons';

const formProps = {
    name: 'file',
    action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
    headers: {
        authorization: 'authorization-text',
    },
    progress: {
        strokeColor: {
            '0%': '#108ee9',
            '100%': '#87d068',
        },
        strokeWidth: 3,
        format: percent => `${parseFloat(percent.toFixed(2))}%`,
    },
    multiple: false
};

interface Props {
    setPhoto(photo:any): void;
}

function getBase64(file) {
    return new Promise((resolve, reject) => {
        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => resolve(reader.result);
        reader.onerror = error => reject(error);
    });
}

function beforeUpload(file, showMsg) {
    const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
    if (!isJpgOrPng && showMsg) {
        message.error('You can only upload JPG/PNG file!');
    }
    return isJpgOrPng;
}


function UploadReady(props: Props) {
    const [fileList, setFileList] = useState()

    const handleChange = async info => {
        let fileList = [...info.fileList];
        fileList = fileList.filter(file => {
            return beforeUpload(file, false)
        });
        if (info.file.status === 'done') {
            message.success(`${info.file.name} file uploaded successfully`);
        } else if (info.file.status === 'error') {
            message.error(`${info.file.name} file upload failed.`);
        }

        fileList = fileList.slice(-1);
        fileList = fileList.map(file => {
            if (file.response) {
                file.url = file.originFileObj;
            }
            return file;
        });
        setFileList(fileList)
        if(fileList[0]) props.setPhoto(await getBase64(fileList[0].originFileObj))
    };

    return (
        <div >
            <Upload name={"photo"} {...formProps} fileList={fileList}
                    onChange={handleChange} beforeUpload={(file)=>beforeUpload(file,true)}>
                <Button icon={<UploadOutlined/>}>Click to Upload</Button>
            </Upload>
        </div>
    );

}

export default UploadReady;