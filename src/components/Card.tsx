import React from "react";
import './Card.scss'
import {cardProps} from "../configs/Props";
import {Image} from 'antd';
import Chart from './Chart'
import CloseIcon from "@material-ui/icons/Close";
import {IconButton} from "@material-ui/core";

interface Props {
    card: cardProps,
    index: number,
    onClick: any,
    activeCardId: number,

    setFastCards(cards: cardProps[]): void;
    deleteCard(card:cardProps):cardProps[];
    setCard(card: cardProps|undefined): void;
}

const setContent = (card: cardProps) => {
    switch (card.type) {
        case 'text':
            return <div>
                <div dangerouslySetInnerHTML={{__html: card.data}}/>
            </div>
        case 'photo':
            return <div>
                <Image
                    className={'photo'}
                    width={"100%"}
                    src={card.data}
                />
            </div>
        case 'video':
            return <div>
                <iframe className={'video'} title={card.title} src={card.data.replace('watch?v=', 'embed/')}
                        frameBorder="0" allowFullScreen/>
            </div>
        case 'chart':
            return <div>
                <Chart data={card.data}/>
            </div>
        default:
            return <p>empty</p>
    }
}

const Card = (props: Props) => {
    const {card} = props;
    let content = setContent(card);

    return (

        <div className={card.id === props.activeCardId ? "card active" : "card"} onClick={props.onClick}>
            <IconButton className={"closeButton"} size={'small'} onClick={async ()=>{
                props.setFastCards(await props.deleteCard(card))
                props.setCard(undefined)
            }}>
                <CloseIcon className={"closeButtonIcon"}/>
            </IconButton>
            <h1 className={"cardHeader"}>{card.title}</h1>
            {content}
        </div>
    )
}

export default Card