import React, {RefObject, useEffect} from "react";
import './Dashboard.scss'
import {Button, Dropdown, Menu, Spin, Tooltip} from 'antd'
import {PlusOutlined} from '@ant-design/icons';
import {TitleOutlined, BarChartOutlined, PhotoOutlined, OndemandVideoOutlined} from '@material-ui/icons'
import {cardProps, dashboardProps} from '../configs/Props'
import Card from './Card'
import {configs} from "../configs/configs";
import {SortableContainer, SortableElement} from 'react-sortable-hoc';
import actions from "../redux/actions";
import {connect} from "react-redux";


interface Props {
    dashboard: dashboardProps;
    cards: cardProps[];
    activeCardId: number;
    fastCards: cardProps[];

    getCards(id: number): cardProps[];
    addCard(card: cardProps): cardProps[];
    changeCardOrder(cards: cardProps[], dashboardId: number): Promise<void>;
    setCard(card: cardProps|undefined): void;
    setFastCards(cards: cardProps[]): void;
    deleteCard(card: cardProps):cardProps[];

}

const btnStyle = {width: "90%", height: "100%"}
const buttons = [
    {icon: <BarChartOutlined style={btnStyle}/>, type: 'chart', hint: 'График'},
    {icon: <OndemandVideoOutlined style={btnStyle}/>, type: 'video', hint: 'Видео'},
    {icon: <PhotoOutlined style={btnStyle}/>, type: 'photo', hint: 'Фото'},
    {icon: <TitleOutlined style={btnStyle}/>, type: 'text', hint: 'Текст'}
]

const dashboardRef: RefObject<any> = React.createRef()

const menu = (createCard: (string) => void) => (
    <Menu>
        {buttons.map(button => (
            <Menu.Item key={button.type}>
                <Tooltip title={button.hint} placement="right">
                    <Button className={'addBtn'} type="primary"
                            shape="circle" icon={button.icon}
                            size={'large'} onClick={() => createCard(button.type)}/>
                </Tooltip>
            </Menu.Item>
        ))}
    </Menu>
);

function Dashboard(props: Props) {
    let {cards, dashboard} = props;
    useEffect(() => {
        (async function () {
            let res = await props.getCards(dashboard.id);
            props.setFastCards(res)
        })();
    }, [dashboard.id])

    if (cards === undefined) return <Spin/>

    const SortableItem = SortableElement(({card, index}) => <Card activeCardId={props.activeCardId} index={index}
                                                                  card={card} deleteCard={props.deleteCard} setCard={props.setCard}
                                                                  setFastCards={props.setFastCards}
                                                                  onClick={() => props.setCard(card)}/>);

    const SortableList = SortableContainer(({cards}) => {
        return (
            <div className={"cards"}>
                {cards && cards.map((card, index) => (
                    <SortableItem key={card.id} index={index} card={card}/>
                ))}
            </div>
        );
    });

    const onSortEnd = ({oldIndex, newIndex}) => {
        if (oldIndex === newIndex) return;
        let newCards = cards.slice()
        const sourceEl = newCards.splice(oldIndex, 1)[0]
        newCards.splice(newIndex, 0, sourceEl)
        props.changeCardOrder(newCards, sourceEl.dashboardId)
        props.setFastCards(newCards)
        props.setCard(newCards[newIndex])

    };


    const scrollToBottom = () => {
        dashboardRef.current.scrollIntoView()
    }

    const createCard = async (type: string) => {
        let lastIndex = cards.reduce((index, card) => {
            return card.id > index ? card.id : index;
        }, 0)

        const newCard: cardProps = {
            dashboardId: dashboard.id,
            id: lastIndex + 1,
            ...configs.defaultCards[type]
        }
        props.setFastCards(await props.addCard(newCard));
        props.setCard(newCard);
        scrollToBottom()
    }

    return (
        <div className="dashboard" style={{backgroundImage: `url("${dashboard.background}")`}}>
            <div className={"header"}><h1>{dashboard.title}</h1></div>
            <SortableList
                distance={1}
                onSortEnd={onSortEnd}
                axis="xy"
                helperClass="SortableHelper"
                cards={props.fastCards}/>
            <Dropdown overlay={menu(createCard)} placement="topCenter" arrow>
                <Button ref={dashboardRef} className={'addBtn'} type="primary"
                        shape="circle" icon={<PlusOutlined/>}
                        size={'large'} onClick={() => createCard("text")}/>
            </Dropdown>
        </div>
    )
}

function mapStateToProps(state) {
    return {
        cards: state.cards,
    }
}

function mapDispatchToProps(dispatch) {
    return {
        getCards: (id: number) => dispatch(actions.cards.getCards(id)),
        addCard: (card: cardProps) => dispatch(actions.cards.addCard(card)),
        changeCardOrder: (cards: cardProps[], dashboardId: number) => dispatch(actions.cards.changeCardOrder(cards, dashboardId)),
        deleteCard: (card:cardProps)=>dispatch(actions.cards.deleteCard(card))
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);